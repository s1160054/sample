require 'factory_girl_rails'
require 'simplecov'

SimpleCov.start do
  add_filter '/vendor/'
  add_filter '/test/'
  add_filter '/config/'
  add_filter '/spec/'
end
Bundler.require
RSpec.configure do |config|
  config.include FactoryGirl::Syntax::Methods
  config.before(:each) do
    DatabaseCleaner.start
  end
end
