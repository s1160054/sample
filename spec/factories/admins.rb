# -*- coding: utf-8 -*-
FactoryGirl.define do
  factory :admin do
    sequence(:login_id){ |i| "my_login_id is_#{i}" }
    sequence(:name)    { |i| "管理者_#{i}" }
    sequence(:password){ |i| "pass_#{i}" }
  end

end
