class CreateLicenses < ActiveRecord::Migration
  def change
    create_table :licenses do |t|
      t.references :customer, index: true
      t.references :application, index: true

      t.timestamps
    end
  end
end
