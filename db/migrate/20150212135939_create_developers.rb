class CreateDevelopers < ActiveRecord::Migration
  def change
    create_table :developers do |t|
      t.references :admin
      t.string :name
      t.string :login_id
      t.string :password

      t.timestamps
    end
  end
end
