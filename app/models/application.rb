class Application < ActiveRecord::Base
  belongs_to :developer
  has_many :customers, through: :licenses
end
