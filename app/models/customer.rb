class Customer < ActiveRecord::Base
  has_many :applications, through: :licenses
end
