class Admin < ActiveRecord::Base
  has_many :developers, dependent: :destroy
  validates :login_id, presence: true, length: {minimum: 6, maximum: 16}, uniqueness: { case_sensitive: false }
  validates :name,     presence: true, length: {minimum: 2, maximum: 20}
  validates :password, presence: true, length: {minimum: 4, maximum: 20}
end
