class Developer < ActiveRecord::Base
  belongs_to :admin
  has_many :applications, dependent: :destroy
end
