class AdminsController < ApplicationController

  def show
    @admin = Admin.find(params[:id])
  end

  def new
    @admin = Admin.new
  end
  def create
    admin = Admin.new(admin_params)
    if admin.save
      redirect_to root_path
    else
      render :new
    end
  end
  private 
  def admin_params
    require(:admin).permit(:name, :login_id, :password)
  end
end
