Rails.application.routes.draw do
  resources :admins, only: [:create, :show, :new]
  root 'main#index'
end
